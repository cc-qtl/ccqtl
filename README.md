[![Anaconda-Server Badge](https://anaconda.org/bioconda/r-ccqtl/badges/version.svg)](https://anaconda.org/bioconda/r-ccqtl)
[![SWH](https://archive.softwareheritage.org/badge/origin/https://gitlab.pasteur.fr/cc-qtl/ccqtl.git/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.pasteur.fr/cc-qtl/ccqtl.git)
[![SWH](https://archive.softwareheritage.org/badge/swh:1:dir:153556ed3046edc62c9e0a863097bb6446ffa4c3/)](https://archive.softwareheritage.org/swh:1:dir:153556ed3046edc62c9e0a863097bb6446ffa4c3;origin=https://gitlab.pasteur.fr/cc-qtl/ccqtl.git;visit=swh:1:snp:99facceddcf74934d98d0b7e0323e83628fc40b9;anchor=swh:1:rev:c27545aebb924da073d4719e902d90683dc7a5c6)
[![Anaconda-Server Badge](https://anaconda.org/bioconda/r-ccqtl/badges/license.svg)](https://anaconda.org/bioconda/r-ccqtl)

# CCQTL

CCQTL is a wrapper around the R/qtl2 (Broman et al, Genetics 2019 10.1534/genetics.118.301595) functions, with hard-coded parameters tailored for QTL mapping in the Collaborative Cross.

Current version of CCQTL builds on the use of qtl2 version 0.30 which is not anymore the default one (0.32, released 2023-04-21). To avoid running into issues during the installation, it is advised to use the following: 

```shell
install.packages("remotes")
library(remotes)
install_version("qtl2", "0.30")
```

More information about ccqtl can be found [here](https://gitlab.pasteur.fr/cc-qtl/cc-qtl-db/-/tree/master#cc-qtl-making-qtl-mapping-in-collaborative-cross-mice-accessible-to-a-broader-users-community).

As of now, only m38 version of mouse genome is supported (incorporation of m39 is underway).

## Make inputs

Create all the files needed for subsequent QTL mapping and later steps (reformat phenotype and covariate files, produce genotype probabilities, allele dosage probabilities and kinship files), generate cross object.

## Genome scan

Perform genome scan using a linear mixed model (single-marker regression with LOCO kinship).

## Permutations

Run permutations.

## Find peaks

Retrieves QTL intervals around LOD peaks that pass permutation-derived significance thresholds.

## Refine regions

Performs local association study and retrieve genes in the QTL interval.

## How to cite ccqtl 
All informations on how to cite ccqtl R package, including its Software Heritage identifier, are provided in CITATION.cff.
